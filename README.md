# robin

A framework that makes making Discord bots very easy.

# Installation

To install Bluebirb.js, you first need to install Node.js.
You can find that here:
https://nodejs.org/en/download/

Once that's done, you should create a folder that will contain your bot inside of it.
Open a command line there. You can do this (In Windows) by Shift+Right-Clicking inside of the folder,
or you can find the folder's path, and type `cd "path here with quotes"` to get to it.
Next, run `npm install bluebirb.js`. This will install bluebirb.js into that folder,
and then you can import it with a simple `var Bluebirb = require("bluebirb.js")` inside of a Node.js file.

# Documentation

I currently don't have any website setup for documentation. But, I have an example-bot project that is on GitLab, which you can find [here](https://gitlab.com/bluebirb/bluebirb-example-bot).
In the readme, it contains most of the major features of Robin, and shows you how to make a working bot with it! (Don't worry, it's very simple code.)