Discord = require("discord.js");

class internalCommandHandler {
  constructor(bot) {
    this.robin = bot;
    this.commands = {};
  }
  init() {
    console.log("Command handler initialized");
    this.robin.addListener("message",msg=>{
      if(msg.author.tag != this.robin.client.user.tag) {
        var executor = msg.content.split(" "); //Array of message split by space
        var args = executor.splice(1,executor.length);
        if(this.commands[executor]) {
          var cmd = this.commands[executor];
          var conditional = (cmd.condition) ? cmd.condition : (()=>{return true});
          if(conditional(msg)) {
            cmd.command.callback(args,msg,this.robin);
          }
        }
      }
    });
  }
  getCommands() {
    return this.commands;
  }
  registerCommand(command,executor,description,condition) {
    //Command is the function to run
    //Executor is what executes the command. e.g. ".help"
    //Description is the description of the command.
    //Condition is optional. A function to run, which if (and only if) it returns true, will run the callback.
    if(!this.commands[executor]) {
      this.commands[executor] = {command,description,condition};
    } else {
      e = new Error(`\x1b[31m Interfering command executors. ${executor} already exists!`);
    }
  }
}

Bot = function() {
  var bot = this;
  this.client = new Discord.Client();
  this.Discord = Discord;

  this.commands = [];
  this.modules = [];
  this.docs = [];
  this.listeners = {};
  this.reactionListeners = {};

  this.addCommand = function(mod,command,description,conditional) {
      this.commands[command] = {
        mod,
        conditional
      }
      a = [];
      a.type = "command";
      a.description = description;
      a.mod = this.commands[command];
      this.modules[command] = a;
  }

  this.addDocumentation = function(command,doc) {
      bot.docs.push({command,doc}); //Will make a command appear in the .help list regardless of if there
      //is an actual command
  }

  this.addListener = function(type,callback,conditional) {
    if(!this.listeners[type]) {
      this.client.on(type,(...args)=>{ //... saves every argument to an array called args
        this.listeners[type].forEach(listener=>{
          if(conditional) {
            if(conditional(listener)) listener.apply(null,args);
          } else listener.apply(null,args);
        }); //apply passes an array as individual arguments
      });
    }
    this.listeners[type] = this.listeners[type] || [];
    this.listeners[type].push(callback);
  }

  this.getModule = function(command) {
    return this.modules[command];
  }

  this.login = function(token) {
    this.client.login(token);
  }

  this.ReactionManager = function(message) {
    this.message = message;
    this.reactions = [];
    this.reactionRemoves = [];

    bot.addListener("messageReactionAdd",(r,u)=>{
      if(this.reactions[r.emoji.toString()] && r.message.id === this.message.id && u.id != bot.client.user.id) this.reactions[r.emoji.toString()](r,u);
      if(this.reactions[r.emoji.id] && r.message.id === this.message.id && u.id != bot.client.user.id) this.reactions[r.emoji.id](r,u);
    });
    bot.addListener("messageReactionRemove",(r,u)=>{
      if(this.reactionRemoves[r.emoji.toString()] && r.message.id === this.message.id && u.id != bot.client.user.id) this.reactionRemoves[r.emoji.toString()](r,u);
      if(this.reactions[r.emoji.id] && r.message.id === this.message.id && u.id != bot.client.user.id) this.reactionRemoves[r.emoji.id](r,u);
    });

    this.onReact = function(r,callback) {
      this.reactions[r] = callback;
      return this;
    }
    this.onRemoveReact = function(r,callback) {
      this.reactionRemoves[r] = callback;
      return this;
    }
    this.addReaction = function(r) {
      this.message.addReaction(r);
      return this;
    }
    this.removeReactions = function(r) {
      reactions = this.message.reactions.array();
      reactions.forEach(reaction=>{
        if(reaction.emoji.toString() === r) {
          reaction.remove();
        }
      });
      return this;
    }
  }

  this.addReactionListener = function(message) {
    if(!this.reactionListeners[message.id]) {
      this.reactionListeners[message.id] = new this.ReactionManager(message);
      return this.reactionListeners[message.id];
    }
  }

  this.addListener("voiceStateUpdate",(pre,post)=>{
    if(pre.voiceChannel != null && post.voiceChannel == null) {
      this.client.emit("userLeaveChannel",post,pre.voiceChannel);
    } else if(pre.voiceChannel == null && post.voiceChannel != null) {
      this.client.emit("userJoinChannel",post,post.voiceChannel);
    } else if(pre.voiceChannel != null && post.voiceChannel != null) {
      this.client.emit("userChangeChannel",post,pre.voiceChannel,post.voiceChannel);
    }
  });

  this.commandHandler = new internalCommandHandler(this);
  bot.addListener("ready",()=>this.commandHandler.init());

  this.onReady = function(callback) {
    this.addListener("ready",callback);
  }
}

module.exports.Bot = Bot;
